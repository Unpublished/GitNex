GitNex is a free, open-source Android client for Git repository management tool Gitea. Gitea is a community managed fork of Gogs, lightweight code hosting solution written in Go.

# Features

- File and directory browser
- File viewer
- Create files
- Login via Token
- Merge pull request
- Offline mode(cache)
- Explore repositories
- Diff view of PR files for public repositories
- My repositories
- Repositories list
- Organizations list
- Create new repository
- Create new organization
- Search/filter repositories and organizations
- Profile view
- Repository stars, watchers, issues count
- Issues list
- Issue comments
- Comment on issues
- Search issues in issues list
- Create new issue with multiple assignee, labels and add milestone, due date to it
- Create label
- Edit / delete labels
- Repository information
- Milestones list
- Create new milestone
- Branches list
- Releases with source download
- Collaborators view for repository
- Markdown support
- Emoji support
- Settings : Pretty and Normal time format, language change
, issue badge
- Option to access local non-https installs
- Basic HTTP authentication support. Use USERNAME@YOUR-DOMAIN.COM in URL field
- 2FA OTP support. Check the Troubleshoot wiki page for usage
- Create a new user - Admin privilege required
- Closed issues list
- Edit issues

More features - https://gitea.com/gitnex/GitNex/wiki/Features

Source code: https://gitea.com/gitnex/GitNex
Developer: https://mastodon.social/@mmarif
